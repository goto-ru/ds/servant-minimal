# servant-minimal

A bare minimum Servant API + client.

It's common for Servant tutorials to demonstrate nontrivial APIs with auto-serialization and other advanced features. This project takes the opposite approach of focusing on bare basics to build understanding from the bottom up.

## Running

```sh
stack build
stack exec servant-minimal-exe # starts a server on port 8080
# on another tty
stack exec servant-minimal-client-exe
```
