{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}

module Api
  ( app
  ) where

import Network.Wai
import Servant (Server, serve)
import Servant.API
import Types


rooth :: Server RootEndpoint
rooth = return "Hello!"

allh :: Server AllEndpoint
allh = return "All!"

handler :: Server API
handler =
        rooth
    :<|> allh


app :: Application
app = serve api handler
