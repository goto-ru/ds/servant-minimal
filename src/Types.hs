{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Types where

import Data.Proxy
import Data.Text (Text)
import qualified Data.Text as T
import Servant.API

type RootEndpoint = Get '[ PlainText] Text
type AllEndpoint = "all" :> Get '[ PlainText] Text

type API =
        RootEndpoint
    :<|> AllEndpoint

api :: Proxy API
api = Proxy
