{-# LANGUAGE DataKinds #-}

module Client where

import Servant
import Servant.Client
import Types

root :<|> all = client api
