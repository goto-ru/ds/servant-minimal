module Main where

import Network.HTTP.Client
import Servant.Client
import Client

main :: IO ()
main = do
    manager <- newManager defaultManagerSettings
    print =<< flip runClientM (ClientEnv manager (BaseUrl Http "localhost" 8080 "")) root
